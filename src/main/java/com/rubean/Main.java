package com.rubean;

import java.io.*;
import java.net.URI;
import java.net.URISyntaxException;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.rubean.data.Data;
import com.rubean.data.Onboarding;
import com.rubean.data.Terminal;
import org.apache.http.HttpEntity;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.HttpResponse;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.util.EntityUtils;

public class Main {

    private static final String restURL = "http://192.168.10.82:8091/api/v3/0-37/onboarding/";
    private static final String email = "palim@mailinator.com";
    private static final String terminalId = "30000010";
    private static final String password = "Test123#";

    public static void main(String[] args) {
        String json = serializeOnboarding();
        HttpEntity httpEntity = sendJsonToURI(json);
        retrieveResponse(httpEntity);
    }

    private static String serializeOnboarding() {
        Terminal terminal = new Terminal(email, terminalId, password);
        Terminal[] terminalArray = {terminal};
        Data data = new Data(terminalArray);
        Onboarding onboarding = new Onboarding(data);
        Gson gson = new Gson();
        return gson.toJson(onboarding);
    }

    private static HttpEntity sendJsonToURI(String json) {
        StringEntity stringEntity = null;
        System.out.println("Rest-URL: " + restURL);
        printJsonPretty("Request:", json);
        try {
            stringEntity = new StringEntity(json);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        try {
            URI uri = new URI(restURL);
            final HttpPost httpPost = new HttpPost(uri);
            httpPost.setEntity(stringEntity);
            httpPost.setHeader("Accept", "application/json");
            httpPost.setHeader("Content-type", "application/json");
            RequestConfig config = handleTimeout();
            final CloseableHttpClient client = HttpClientBuilder.create().setDefaultRequestConfig(config).build();
            HttpResponse httpResponse = client.execute(httpPost);
            return httpResponse.getEntity();

        } catch (URISyntaxException | IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    private static void retrieveResponse(HttpEntity entity) {
        String result = null;
        try {
            result = EntityUtils.toString(entity);
        } catch (IOException e) {
            e.printStackTrace();
        }
        printJsonPretty("Response:", result);
    }

    private static RequestConfig handleTimeout() {
        int timeout = 30;
        return RequestConfig.custom()
                .setConnectTimeout(timeout * 1000)
                .setConnectionRequestTimeout(timeout * 1000)
                .setSocketTimeout(timeout * 1000).build();
    }

    private static void printJsonPretty(String description, String json){
        System.out.println(description);
        Gson gsonBuilder = new GsonBuilder().setPrettyPrinting().create();
        JsonElement je = JsonParser.parseString(json).getAsJsonObject();
        System.out.println(gsonBuilder.toJson(je));
        System.out.println();
    }

}
