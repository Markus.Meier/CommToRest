package com.rubean.data;

public class Terminal {
    private String email = "";
    private String terminalId = "";
    private String password = "";

    public Terminal(String email, String terminalId, String password) {
        this.email = email;
        this.terminalId = terminalId;
        this.password = password;
    }
}
